# COMANDOS GIT
 

 ### Comandos Git aprendidos hasta ahora:

-git init
  * para iniciar un repositorio en un directorio del equipo
- git add .  
  * para añadir archivos al STAGING AREA  
- git add *.txt   (.pdf , .html ,etc.)
  * para añadir archivos filtrados por extension al STAGING AREA
- git commit -m "comentario de turno "
  * para guardar los archivos de STAGING AREA con un comentario identificativo
- git push
  * para subir los archivos a GitHub
- git pull
  * para descargarse la ultima version del repositorio
- git clone + url
  * para sincronizar el repositorio en tu directorio local
- git status -s
  * para identificar el estado del directorio local y del staging area
- git log --oneline ( más el código)
  * para identificar los diferentes commits realizados, 
- git reset --hard ( más el código)
  * para volver a un estado del repositorio anterior a traves de la linea temporal de los commits
- git branch
  * para identificar cual es la rama del repositorio en la que estamos trabajando